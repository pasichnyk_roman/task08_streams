package com.epam.Task1;

public class App {

    public static void main(String[] args) {

        Calculate max = (a, b, c) -> Math.max(a, Math.max(b, c));

        int resultMax = max.calculate(15, 25, 11);
        System.out.println(resultMax);

        Calculate avg = (a, b, c) -> (a + b + c) / 3;
        int resultAvg = avg.calculate(10, 20, 15);
        System.out.println(resultAvg);

    }

}
